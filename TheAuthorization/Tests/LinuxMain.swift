import XCTest

import TheAuthorizationTests

var tests = [XCTestCaseEntry]()
tests += TheAuthorizationTests.allTests()
XCTMain(tests)
