import XCTest

#if !canImport(ObjectiveC)
public func allTests() -> [XCTestCaseEntry] {
    return [
        testCase(TheAuthorizationTests.allTests),
    ]
}
#endif
