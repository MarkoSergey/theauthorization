import XCTest
@testable import TheAuthorization

final class TheAuthorizationTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(TheAuthorization().text, "Hello, World!")
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
