// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "TheAuthorization",
    platforms: [.iOS(.v12)],
    products: [
        .library(
            name: "TheAuthorization",
            targets: ["TheAuthorization"]),
    ],
    dependencies: [.package(url: "https://github.com/jrendel/SwiftKeychainWrapper", from: "4.0.1")
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
    ],
    targets: [
        .target(name: "TheAuthorization",
                dependencies: ["SwiftKeychainWrapper"]),
    ]
)
