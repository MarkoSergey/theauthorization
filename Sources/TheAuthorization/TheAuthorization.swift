import SwiftKeychainWrapper
import UIKit

public struct TheAuthorization {
    
    public static let standart = TheAuthorization()
    private init() {}
    
    public enum AuthError: Error {
        case emailAlreadyExists
        case wrongEmailOrPassword
    }

    
    public func signUpAccount(email: String, password: String, completion: @escaping (Result<Bool, AuthError>) -> Void) {
        
        let emailKeys = KeychainWrapper.standard.allKeys()
        
        if !emailKeys.contains(email) {
            _ = KeychainWrapper.standard.set(password, forKey: email)
            completion(.success(true))
        } else {
            completion(.failure(.emailAlreadyExists))
        }
    }
    
    public func logInAccount(email: String, password: String, completion: @escaping (Result<Bool, AuthError>) -> Void) {
    
        let passwordKey = KeychainWrapper.standard.string(forKey: email)
        let emailKeys = KeychainWrapper.standard.allKeys()
        
        if passwordKey == password && emailKeys.contains(email) {
            completion(.success(true))
        } else {
            completion(.failure(.wrongEmailOrPassword))
        }
    }
}
